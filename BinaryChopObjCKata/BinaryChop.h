#import <Foundation/Foundation.h>

@interface BinaryChop : NSObject

+ (int)chop:(int)num fromArray:(NSArray *)set;

@end