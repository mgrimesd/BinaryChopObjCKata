#import "BinaryChop.h"

@implementation BinaryChop

// The easiest approach is to let NSArray do the heavy lifting and just ensure the wrapper
// does what we want through tests.
+ (int)chop:(int)num fromArray:(NSArray *)set {
    if ([set containsObject:[NSNumber numberWithInt:num]])
        return [set indexOfObject:[NSNumber numberWithInt:num]];
    return -1;
}

@end