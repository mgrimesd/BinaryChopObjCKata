#import "TestHelper.h"
#import "BinaryChop.h"

SpecBegin(BinaryChopSpec)

describe(@"A BinaryChop", ^{

    it(@"should return a negative one for any arbitrary value if the search set is empty", ^{
        expect([BinaryChop chop:42 fromArray:@[]]).to.equal(-1);
        expect([BinaryChop chop:0 fromArray:@[]]).to.equal(-1);
        expect([BinaryChop chop:-42 fromArray:@[]]).to.equal(-1);
    });

    it(@"should return a negative one for a value not found in a set of 1", ^{
        expect([BinaryChop chop:2 fromArray:@[@4]]).to.equal(-1);
        expect([BinaryChop chop:0 fromArray:@[@-1]]).to.equal(-1);
    });

    it(@"should return a negative one for a value not found in a set more than 1", ^{
        NSArray *setOfTwo = @[@4, @2];
        expect([BinaryChop chop:0 fromArray:setOfTwo]).to.equal(-1);
        expect([BinaryChop chop:-1 fromArray:setOfTwo]).to.equal(-1);
    });

    it(@"should return a zero for a value found in the first position", ^{
        expect([BinaryChop chop:4 fromArray:@[@4]]).to.equal(0);
    });

    it(@"should return a one for a value found in the second position", ^{
        NSArray *setOfTwo = @[@4, @2];
        expect([BinaryChop chop:2 fromArray:setOfTwo]).to.equal(1);
    });

    it(@"should return a two for a value found in the third position", ^{
        NSArray *setOfFive = @[@4, @3, @2, @1, @0];
        expect([BinaryChop chop:2 fromArray:setOfFive]).to.equal(2);
    });

});

SpecEnd